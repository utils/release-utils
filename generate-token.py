#!/usr/bin/env python3
'''
Generate a token for developers to act as release users

This script creates API tokens for developers to perform actions as the release
user for a project.

The only required flags are:

  - `-t`: the GitLab API token
  - `-u`: the developer's user account

Optional flags include:

  - `-s`: the token is an admin token
  - `-p`: when using an admin token and `--sudo`, create a token for this
    project
  - `-g`: the GitLab instance to communicate with (defaults to
    `gitlab.kitware.com`)

This script prints the token and its expiration date.
'''

import argparse
import datetime
import requests


KNOWN_PROJECTS=(
    'cmb',
    'paraview',
    'vtk',
    'vtk-m',
)


def options():
    p = argparse.ArgumentParser(
        description='Create a token for a user to create release MRs')
    p.add_argument('-g', '--gitlab', type=str, default='gitlab.kitware.com',
                   help='the gitlab instance to communicate with')
    p.add_argument('-t', '--token', type=str, required=True,
                   help='API token')
    p.add_argument('-s', '--sudo', action='store_true',
                   help='use `sudo` to act as the project release user')
    p.add_argument('-p', '--project', type=str,
                   help='the project to create the token for')
    p.add_argument('-u', '--user', type=str, required=True,
                   help='the user performing the release')
    return p


class Gitlab(object):
    def __init__(self, gitlab_url, token):
        self.gitlab_url = f'https://{gitlab_url}/api/v4'
        self.headers = {
            'PRIVATE-TOKEN': token,
        }

    def get(self, endpoint, **kwargs):
        rsp = requests.get(f'{self.gitlab_url}/{endpoint}',
                           headers=self.headers,
                           params=kwargs)

        if rsp.status_code != 200:
            raise RuntimeError(f'Failed request to {endpoint}: {rsp.content}')

        return rsp.json()

    def post(self, endpoint, **kwargs):
        rsp = requests.post(f'{self.gitlab_url}/{endpoint}',
                            headers=self.headers,
                            params=kwargs)

        if rsp.status_code != 201:
            raise RuntimeError(f'Failed post to {endpoint}: {rsp.content}')

        return rsp.json()


if __name__ == '__main__':
    p = options()
    opts = p.parse_args()

    gitlab = Gitlab(opts.gitlab, opts.token)

    if opts.sudo:
        if opts.project is None:
            raise RuntimeError('Using `--sudo` requires specifying the project name')
        if opts.project not in KNOWN_PROJECTS:
            raise RuntimeError(f'Unknown release project: {opts.project}')

        release_user = f'kwrobot.release.{opts.project}'
        users = gitlab.get('users',
            username=release_user)
        if not users:
            raise RuntimeError(f'Unknown release user {release_user}')
        user = users[0]

        today = datetime.datetime.today().date()
        four_weeks = datetime.timedelta(weeks=4)
        expiration = today + four_weeks

        new_token = gitlab.post(f'users/{user["id"]}/personal_access_tokens',
            name=opts.user,
            expires_at=expiration.strftime('%Y-%m-%d'),
            scopes=['api'])
    else:
        raise RuntimeError('GitLab does not yet support creating API tokens from the API')

        new_token = gitlab.post('users/personal_access_tokens',
            name=opts.user,
            scopes=['api'])

    print(f'token: {new_token["token"]}')
    print(f'expiration: {new_token.get("expires_at")}')
